###


![alt text](screenshots/01.png)

### Usage
```html

      <datatable [columns]="columns" [rows]="rows" [menus]="menus"  [options]="myOrderOption" [buttons]="buttons">
```

### Features

### Examples

##### *.component.ts
```typescript
@Component(/**/)
export class Component implements OnInit {
   columns: Column[] = [];
    rows: any[] = [];
    menus: Menu[] = [];
    buttons: Button[] = [];

    editShow: boolean;
    editId:number;

    detailShow: boolean;
    detailId:number;

    deleteShow: boolean;
    deleteId:number;

    id: number;
    cancelShow: boolean;


    selectedItem:any = { value: null, text: '' };
    selectedItems:any[] = [];
    items: any[];

    title = 'GlobalStructures';


    myOrderOption: DataTableOption = new DataTableOption;

    public constructor(private router: Router, private route: ActivatedRoute) { }
    ngOnInit(): void {

      this.myOrderOption.create = true;
      this.myOrderOption.edit = true;
      this.myOrderOption.delete = true;
      this.myOrderOption.excel = true;
      this.myOrderOption.print = true;
      this.myOrderOption.save = true;
      this.myOrderOption.columns = true;
      this.myOrderOption.limits = true;
      this.myOrderOption.search = true;
      this.myOrderOption.sort = true;
      this.myOrderOption.paging = true;
      this.myOrderOption.info = true;
      this.myOrderOption.titleColumns = true;
      this.myOrderOption.classStyle ='';


      this.columns.push(new Column('text1 ', 'id'));
      this.columns.push(new Column(' text2', 'name'));
      this.columns.push(new Column('  text3', 'requests'));


      this.rows = [];
      for (let i = 0; i < 10; i++) {
        this.rows.push({ name: '  متن' +'('+ i+')',requests:'متن'});
      }

      let menu = new Menu;
      menu.items.push(new MenuItem('ویرایش', row => this.edit(row.id), 'mdi mdi-pencil', 'btn gray'));
      menu.items.push(new MenuItem('حذف', row => this.delete(row.id), 'mdi mdi-delete', 'btn gray'));
      this.menus.push(menu);




    }





}
```

