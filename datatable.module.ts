import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Route } from '@angular/router';
import { Http, HttpModule } from '@angular/http';
import { DataTableComponent } from './components/datatable.component';
import { PagingPipe } from './pipes/paging.pipe';
import { SortPipe } from './pipes/sort.pipe';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    HttpModule,
    RouterModule,

  ],
  declarations: [
    DataTableComponent,

    PagingPipe,
    SortPipe,


  ],
  exports: [
    DataTableComponent,
  ]
})
export class DataTableModule { }
