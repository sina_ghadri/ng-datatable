export class Button {
    public text: string;
    public className: string;
    public icon: string;
    public onclick: () => void;

    public constructor(text: string, onclick: () => void, icon: string = '', className: string = '') {
        this.text = text;
        this.icon = icon;
        this.onclick = onclick;
        this.className = className;
    }
}