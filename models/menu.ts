import { Router } from '@angular/router';
export class MenuItem {
    public text: string;
    public className: string;
    public icon: string;
    public tooltip:boolean = true;
    public onclick: (row: any) => void;

    public constructor(text: string, onclick: (row: any) => void, icon: string = '', className: string = '') {
        this.text = text;
        this.icon = icon;
        this.onclick = onclick;
        this.className = className;
    }
}
export enum MenuType { Default, HoverMenu, HoverInline, DropDown }
export class Menu {
    public type: MenuType = MenuType.Default;
    public items: MenuItem[] = [];

    public getClass() {
        let className = '';
        if (this.items.length == 0) className += 'hide ';
        switch (this.type) {
            case MenuType.HoverMenu: className += 'hover menu '; break;
            case MenuType.HoverInline: className += 'hover inline '; break;
            case MenuType.DropDown: className += 'dropdown '; break;
            case MenuType.Default: className += 'default '; break;
        }
        return className;
    }
    public addEdit(router:Router): Menu { 
        let item = new MenuItem('ویرایش', (row) => { router.navigateByUrl('../edit/' + row.id); }, 'mdi mdi-pencil', 'btn blue');
        this.items.push(item);
        return this; 
    }
    public addDelete(router:Router): Menu { 
        this.items.push(new MenuItem('حذف', (row) => { router.navigateByUrl('../delete/' + row.id); }, 'mdi mdi-delete', 'btn red'));
        return this; 
    }
}