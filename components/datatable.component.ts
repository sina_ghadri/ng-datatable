import { Component, OnInit, Input } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Column } from "../models/column";
import * as XLSX from 'xlsx';
import { Menu, MenuItem } from "../models/menu";
import { Options } from "../models/options";
import { DateFormatter } from "../../extentions/date";
import { Download } from "../../libs/download";
import { Button } from "../models/button";

@Component({
    selector: 'datatable',
    templateUrl: './datatable.component.html',
    styleUrls: ['./datatable.component.css']
})
export class DataTableComponent implements OnInit {
    @Input() name: string = location.pathname.replace(/\//g, '_');
    @Input() options: Options = new Options();
    @Input() columns: Column[];
    @Input() rows: any[] = [];
    @Input() limits: number[];
    @Input() menus: Menu[];
    @Input() buttons: Button[];

    pages: number[] = [];

    page: number;
    sort: string;
    ascending: boolean = true;
    search: string = '';
    limit: number;
    count: number = 0;

    public constructor(private router: Router, private route: ActivatedRoute) { }
    ngOnInit(): void {
        if (this.limits == undefined) this.limits = [10, 15, 25, 50];

        if (this.menus == undefined) {
            this.menus = [];
            let menu = new Menu;
            if (this.options.edit) menu.addEdit(this.router);
            if (this.options.delete) menu.addDelete(this.router);
            if(menu.items.length) this.menus.push(menu);
        }

        if (this.buttons == undefined) {
            this.buttons = [];
            if (this.options.create) {
                this.buttons.push(new Button('ثبت ردیف جدید', () => {
                    this.router.navigate(['../create'], { relativeTo: this.route });
                }, 'mdi mdi-plus', 'btn blue'));
            }
        }

        this.limit = this.limits[0];
        this.page = 1;

        this.setSort(this.columns[0].field);

        this.load();
    }

    filterSort(array: any[]): any[] {
        return array.sort((n1, n2) => {
            if (n1[this.sort] > n2[this.sort]) return (this.ascending ? 1 : -1);
            if (n1[this.sort] < n2[this.sort]) return (this.ascending ? -1 : 1);
            return 0;
        });
    }
    filterSearch(array: any[]): any[] {
        let search = this.search.toLowerCase();
        return array.filter(row => {
            if (search.length == 0) return true;
            for (let i = 0;i < this.columns.length;i++) {
                let value = this.columns[i].getValue(row);
                if (value && value.toLowerCase().indexOf(search) > -1) return true;
            }
            return false;
        })
    }
    filterPaging(array: any[]): any[] {
        let temp: any[] = [];
        for (let i = this.getFrom() - 1, to = i + this.limit, length = array.length; i < to && i < length; i++) temp.push(array[i]);
        return temp;
    }

    getRows(): any[] {
        let array = this.rows;
        array = this.filterSearch(array);
        array = this.filterSort(array);

        this.count = array.length;

        if (this.options.paging) array = this.filterPaging(array);
        return array;
    }
    getCount(): number { return this.count; }
    getPageCount(): number { return this.getCount() / this.limit; }
    getPages(): number[] {
        let pages: number[] = [];
        let count = this.getPageCount();
        for (let i = 0; i < count; i++) pages.push(i + 1);
        return pages;
    }
    getFrom(): number { return (this.page - 1) * this.limit + 1; }
    getTo(): number { let to = this.page * this.limit; return (this.count < to ? this.count : to); }

    setSort(field: string) {
        if (this.options.sort) {
            if (this.sort == field) this.ascending = !this.ascending;
            else { this.sort = field; this.ascending = true; }
        }
    }
    setPage(page: number) { if (page > 0 && page <= this.getPageCount() + 1) this.page = page; }

    prevPage() { if (this.page > 1) this.page--; }
    nextPage() { if (this.page < this.getPageCount()) this.page++; }

    save() {
        let obj = {
            limit: this.limit,
            sort: this.sort,
            ascending: this.ascending
        };
        localStorage.setItem(this.name, JSON.stringify(obj));
    }
    load() {
        let obj = JSON.parse(localStorage.getItem(this.name));
        if (obj) {
            if (this.limits.indexOf(+obj.limit) > -1) this.limit = +obj.limit;
            this.sort = obj.sort;
            this.ascending = obj.ascending;
        }
    }
    exportExcel() {
        let array: any[] = [];
        let temp: string[] = [];
        for (let i = 0; i < this.columns.length; i++) temp.push(this.columns[i].title);
        array.push(temp);
        for (let i = 0; i < this.rows.length; i++) {
            temp = [];
            for (let j = 0; j < this.columns.length; j++) temp.push(this.columns[j].getValue(this.rows[i]));
            array.push(temp);
        }
        Download.xlsx(array, this.name + '_' + DateFormatter.format(new Date(), 'YYYY/MM/DD hh:mm:ss'));
    }
}