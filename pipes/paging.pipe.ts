import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'paging'
})
export class PagingPipe implements PipeTransform {
    transform(rows: any[], from: number, count: number) {
        let array:any[] = [];
        if(from != undefined)
        {
            for(let i = from - 1,to = i + count,length = rows.length;i < to && i < length;i++) array.push(rows[i]);
        }
        return array;
    }
}