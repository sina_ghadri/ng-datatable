import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'sort'
})
export class SortPipe implements PipeTransform {
    transform(rows: any[], field: number, type: boolean) {
        return rows.sort((n1, n2) => {
            if (n1[field] > n2[field]) return (type ? 1 : -1);
            if (n1[field] < n2[field]) return (type ? -1 : 1);
            return 0;
        });
    }
}